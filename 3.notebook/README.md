# DIFFERENCE BETWEEN ALGORITHMS

## SARSA vs Q-LEARNING

### Policy π

Q_Learning is using different policies for choosing next action A' and updating Q (i.e is trying to evaluate π while following another policy µ -> it's an off-policy algorithm)
SARSA uses π all the time, it's an on-policy algorithm

### Updating Q

SARSA uses the Q' following a epsilon-greedy policy exactly as A' is drawn from it
Q-learning uses the maximum Q' over all possible actions for the next step, making it look like following a greedy policy with epsilon=0 (No exploration)
