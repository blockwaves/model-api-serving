import gym
import numpy as np
import time
import pprint

def fit(env):
    # Useful variables
    steps = 0
    penalties = 0
    done = False
    total_rewards = []

    # Try until game is over
    while not done:
        t0 = time.time()
        action = env.action_space.sample()
        state, reward, done, info = env.step(action)
        total_rewards.append(reward)

        # Add penalties if unauthorized action (dropoff / pickup)
        if reward == -10:
            penalties += 1

        steps += 1
    
    # Algorithm infos output
    return_dict = {
        "steps" : steps,
        "runtime" : time.time() - t0,
        "total_rewards" : total_rewards,
        "penalities" : penalties
    }   

    return return_dict
