import matplotlib.pyplot as plt
import numpy as np

def statMetrics(statsData, plot=False):
    # [r_game1, r_game2, ... , r_gameN]
    if plot:
        fig, ax = plt.subplots()  
        x = [i for i in range(len(statsData))]
        y = statsData
        ax.plot(x, y); 
        plt.show()

    metrics = {
        "q1" : np.quantile(statsData,.25),
        "median" :  np.quantile(statsData,.50),
        "q3" : np.quantile(statsData,.75),
        "max" : max(statsData),
        "min" : min(statsData)

    }

    return metrics

def minmaxMetrics(timePerGame):
    # [time_game1, time_game2, ... , time_gameN]
 
    min_time = float("+inf")
    min_time_game_id = 0

    max_time = float("-inf")
    max_time_game_id = 0

    for id, val in enumerate(timePerGame):
        if val < min_time:
            min_time = val
            min_time_game_id = id
        
        if val > max_time:
            max_time = val
            max_time_game_id = id

    metrics = {
        "min" : min_time,
        "min_game" : min_time_game_id,
        "max" : max_time,
        "max_game" : max_time_game_id,
    }

    return metrics

    

def rateMetrics(winLoseGame):
    wins, loss = 0, 0
    for i in winLoseGame:
        if bool(i):
            wins+=1 
        else: 
            loss+=1

    rates = {
        "winrate" : wins / len(winLoseGame),
        "lossrate" : loss / len(winLoseGame),
        "winrate_%" : round(wins / len(winLoseGame)*100),
        "lossrate_%" : round(loss / len(winLoseGame)*100)
    }

    return rates



