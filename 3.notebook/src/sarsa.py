# Librairies
import pdb
from unittest import result
import numpy as np
import gym
import time
from tqdm import tqdm
import optuna

def fit(alpha, epsilon, gamma, nb_games, max_steps):
    # Defining utility functions to be used n the learning process
    # Function to choose the next action
    def choose_action(state):
        action = 0
        if np.random.uniform(0, 1) < epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(Q[state, :])
        return action

    # Function to learn the Q-value
    def update(state, state2, reward, action, action2):
        predict = Q[state, action]
        target = reward + gamma * Q[state2, action2]
        Q[state, action] = Q[state, action] + alpha * (target - predict)

    # Initializing the Q-matrix
    Q = np.zeros((env.observation_space.n, env.action_space.n))
    # Training the learning agent
    # Initializing the reward
    reward = 0
    # Initializing metrics array
    all_games_rewards = []
    all_games_steps_counts = []
    all_games_times = []

    # Starting the SARSA learning
    for episode in tqdm(range(nb_games)):
        t0 = time.time()
        steps_count, reward1 = 0, 0
        rewards = []
        t = 0
        state1 = env.reset()
        action1 = choose_action(state1)

        while t < max_steps:
            # Visualizing the training
            # env.render()

            # Getting the next state
            state2, reward, done, info = env.step(action1)

            # Choosing the next action
            action2 = choose_action(state2)

            # Learning the Q-value
            update(state1, state2, reward, action1, action2)

            state1 = state2
            action1 = action2

            # Updating the respective vaLues
            t += 1
            reward += 1

            # If at the end of learning process
            next_state, reward, done, info = env.step(action1)
            if done:
                # if steps_count < 5:
                #     pdb.set_trace(header="Anomaly")
                break

            # Update metrics
            rewards.append(reward1)
            steps_count += 1

        # Add updated metrics to array
        all_games_rewards.append(np.mean(rewards))
        all_games_steps_counts.append(steps_count)
        all_games_times.append(time.time()-t0)

    # Return the dicts containing the metrics updated
    return_dict = {}
    return_dict["rewards"] = all_games_rewards
    return_dict["steps"] = all_games_steps_counts
    return_dict["games_times"] = all_games_times
    return_dict["Q_table"] = Q

    return(return_dict)

def play(alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000, max_steps = 100, train_mode="opti"):
    def objective(trial):
        results = fit(trial.suggest_float("alpha", 0, 1 ), 
            trial.suggest_float("epsilon", 0, 1 ), 
            trial.suggest_float("gamma", 0, 1 ), 
            trial.suggest_int("nb_games", 10000, 30000 ), 
            trial.suggest_int("max_steps",50, 150)
        )

        return min(results["steps"])

    if train_mode == "opti":
        study = optuna.create_study(direction="minimize")
        study.optimize(objective, n_trials=5)

        alpha = study.best_params["alpha"]
        epsilon = study.best_params["epsilon"]
        gamma = study.best_params["gamma"]
        nb_games = study.best_params["nb_games"]
        max_steps = study.best_params["max_steps"]

    results = fit(alpha, epsilon, gamma, nb_games, max_steps)

    return results


if __name__ == '__main__':
    # Train
    env = gym.make("Taxi-v3")
    results = play(env)
   
    # Run the game !
    env.reset()
    steps = 0
    done = False

    while not done:
        env.render()
        time.sleep(1.5)
        steps += 1
        state = env.s
        action = np.argmax(results["Q_table"][state]) 
        next_state, reward, done, info = env.step(action) 
        import os
        os.system('clear')
  
    print(f"This game was resolved in { steps } steps")