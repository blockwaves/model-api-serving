# Librairies
import pickle
from tensorflow import keras 
from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding, Reshape, Flatten
from keras.optimizers import Adam
from rl.agents.dqn import DQNAgent
from rl.policy import EpsGreedyQPolicy
from rl.memory import SequentialMemory
import pandas as pd
import numpy as np
import optuna
import pdb
import gym

#
# @CLASS DQN_Taxi
#
class DQN_Taxi:    
    
    
    # @method __init__
    # @param  env 
    # @return void
    def __init__(self, env,  SequentialMemoryLimit=100000, 
                    nb_actions=6, nb_steps_warmup=500, learning_rate=1e-3, 
                    metrics=['mae']):
        self.env = env
        self.init_model()
        memory = SequentialMemory(limit=SequentialMemoryLimit, window_length=1)
        policy = EpsGreedyQPolicy()
        self.agent = DQNAgent(model=self.model, nb_actions=nb_actions, 
                            memory=memory, nb_steps_warmup=nb_steps_warmup, 
                            target_model_update=1e-2, policy=policy)
        self.agent.compile(Adam(learning_rate=learning_rate), metrics=metrics)
        
        
    # Loading the model
    # @method load_model
    # @param  model_path  The location of the model
    # @return void
    def load_model(self, model_path="./DQN_data/model"):
        self.model = keras.models.load_model(model_path)

        
        
    # Training the model
    # @method train_model
    # @param  save_model  Save the model once training done ? saving the model will 
    #                          also save the history in a csv file (default : True)
    # @param  model_path  Where to save the model ? (default saved as "model")
    # @param  SequentialMemoryLimit  The maximum rows stored in memory (default : 50000)
    # @param  verbose  Show the logs ? (default : True)
    # @param  visualize  Watch the DQN agent training  (default : False)
    # @param  nb_actions  Number of actions
    # @param  nb_steps_warmup  Maximum number of states
    # @param  learning_rate  The model learning rate (default : 1e-3)
    # @param  metrics  The metric is a function that is used to judge the performance of your model
    # @param  nb_steps  Number of steps to train the model (default : 1000000)
    # @param  nb_max_episode_steps  The maximum number of steps per episodes (default : 99)
    # @param  log_interval  Interval of logs (default : 100000)
    # @return void
    def train_model(self, save_model=True, model_path="../DQN_data/model", 
                    verbose=True, visualize=False, nb_steps=1000000, nb_max_episode_steps=99, log_interval=100000):
       
        self.history = self.agent.fit(env, nb_steps=nb_steps, visualize=visualize, verbose=verbose, 
                               nb_max_episode_steps=nb_max_episode_steps, log_interval=log_interval)

        if save_model == True:
            self.save_model(model_path)
            self.save_history_dataframe(model_path)
        
        return self.history
    
    # Initialize a Sequential model
    # @method init_model  
    # @return void
    def init_model(self):
        model = keras.Sequential()
        model.add(Embedding(500, 10, input_length=1))
        model.add(Reshape((10,)))
        model.add(Dense(50, activation='relu'))
        model.add(Dense(50, activation='relu'))
        model.add(Dense(50, activation='relu'))
        model.add(Dense(6, activation='linear'))
        self.model = model
        
        
    # Return the history as a pandas Dataframe
    # @method history_dataframe  
    # @return dataframe or false
    def history_dataframe(self):
        if(self.history):
            return pd.DataFrame(self.history.history) 
        return False 
    
    
    # Show the model summary
    # @method model_summary  
    # @return void
    def model_summary(self):
        self.model.summary()
    
    
    # Save the model
    # @method save_model  
    # @param  model_path
    # @return void        
    def save_model(self, model_path):
        self.model.save(model_path)
    
    
    # Save the history in a csv file
    # @method save_history_dataframe  
    # @param  path
    # @return void        
    def save_history_dataframe(self, path):
        self.history_dataframe().to_csv(path + '/history.csv')


    def load(self, weight_path="3.notebook/DQN_data/model_1m/dqn_weights.h5f"):
        self.agent.load_weights(weight_path)

    def test(self,nb_episodes=1, visualize=True, nb_max_episode_steps=99):
        self.agent.test(self.env, nb_episodes=nb_episodes, visualize=visualize, nb_max_episode_steps=nb_max_episode_steps)


def params_opti(agent, SequentialMemoryLimit=100000, 
                    verbose=True, visualize=False, nb_actions=6, nb_steps_warmup=500, 
                    metrics=['mae'], log_interval=100000):
                    
        def objective(trial):
            # Hyperparameters
            nb_max_episode_steps=trial.suggest_int("nb_max_episode_steps",50,150)
            nb_steps=trial.suggest_int("nb_steps",100,200)
            learning_rate= trial.suggest_float("learning_rate",1e-5,1e-2)#1e-3
            
            # Train it
            memory = SequentialMemory(limit=SequentialMemoryLimit, window_length=1)
            policy = EpsGreedyQPolicy()
            dqn = DQNAgent(model=agent.model, nb_actions=nb_actions, 
                                memory=memory, nb_steps_warmup=nb_steps_warmup, 
                                target_model_update=1e-2, policy=policy)
            dqn.compile(Adam(learning_rate=learning_rate), metrics=metrics)
            history = dqn.fit(env, nb_steps=nb_steps, visualize=visualize, verbose=verbose, 
                                nb_max_episode_steps=nb_max_episode_steps, log_interval=log_interval)    
            dqn.dqn = dqn
            dqn.history = history
            # return min(history.history["episode_reward"]) # maximize the study direction
            return min(history.history["nb_episode_steps"])
            

        study = optuna.create_study(direction='minimize')
        study.optimize(objective, n_trials=5)

        print(study.best_params)
        pdb.set_trace()
        return study.best_params

if __name__ == '__main__':
    # Train
    env = gym.make("Taxi-v3").env
    env.reset()
    DQN = DQN_Taxi(env)
    print(params_opti(DQN))

    # DQN.load()
    # DQN.test()