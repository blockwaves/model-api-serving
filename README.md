# ai_taxisolver
## Description

The Taxi Solver app is designed to tackle the problem of finding the best route for a taxi to take to a destination.
Our solution include a top of edge user interface, so you know what you are doing.
**Your way, our expertise. Drive safe!**

## Contents
1. client requirements
1. documentation
1. notebook
1. frontend app
1. backend app
1. database app


### Implemented algorithms
- Brute Force
- Q-Learning
- Sarsa 
- Monte-Carlo
- Deep Q-Learning (DQN)

### Technical abstract:
This project is about implementing Renforcement Learning.


## Deployment
You can deploy locally or remotely our app using containers thanks to Docker-Compose.
```s
docker -v
docker-compose -v
docker-compose up --build
```
*It is best to build and run separately and/or force build*
**warning microservices must be configured to PROD mode in order to be exposed publicly**

## Workflows
### Conda
Conda will serve as a wrapper of pip, this way our docker would use pip as usual but our environment would still be contained wihtin a conda env.  
*example*
```
cd 5.frontend
conda env create -f environment.yml
conda activate ai_nlp_travel
```

### GLOBAL CONDA INSTALL (LAZY) - local
```s
conda env create -f environment.yml
conda activate ai_taxisolver
```
And enjoy!

#### missing a dep? 

run `conda install requests -c conda-forge`
conda is a pre-containerization step before packaging the app into a docker image (which don't require conda or moni-conda because... well it's a container!)

### Docker
```
chmod u+x localdockerbuild.sh
./localdockerbuild.sh
```
docker -v
docker-compose -v

## Gitops features {Not seen elsewhere, FREE COOKIE}
- clean notebook : 
    - append to .git/config `[filter "remove-notebook-output"]
    clean = "jupyter nbconvert --clear-output --to=notebook --stdin --stdout --log-level=ERROR"`
    - create a gitattributes files with `*.ipynb clean = 1`


