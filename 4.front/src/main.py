"""
main python file for the app
it is designed as a single file for faster debugging and if possible avoiding lifecycle issues
"""


# dependencies
import os
from dash import Dash, html
# import dash_bootstrap_components as dbc

# internal modules
import dashboard
import dashboard_callback
import config


###############################################################################
#                                MAIN                                         #
###############################################################################

# exposing the instance
app = Dash(__name__ )

app.title = "Taxi Solver UI"

app.layout = dashboard.layout
main_callbacks = dashboard_callback


# if env file not found define default env
# plop

if __name__ == "__main__":
    app.run_server(debug=config.debug, host=config.host, port=config.port)
