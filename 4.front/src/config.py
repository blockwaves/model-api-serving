"""
Defines global variables
os can be used to define global root path (can be a source of issues for tests)
"""
import os
# import logging
import re

# App settings
name = "dashboard taxi solver"

host = "0.0.0.0"

port = int(os.environ.get("PORT", 8800))

debug = True

fontawesome = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'

# File system
# root = os.path.dirname(os.path.dirname(__file__)) + "/src"

# authenticate
password_regex = re.compile('^(?=\S{6,20}$)(?=.*?\d)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^A-Za-z\s0-9])')
error_password = '''The password needs to contain:
Min length is 6 and max length is 20, 
at least include a digit number, an uppercase, 
a lowercase letter and a special characters.'''


# routes name
dashboard_url = '/dashboard'
 
