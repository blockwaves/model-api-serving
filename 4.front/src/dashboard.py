"""
main view of the app
show graph plotting
the layout is divided into 12 columns
"""
# Main dependancies
from dash import dcc, html, Input, Output, callback, dash, State
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn import datasets
from sklearn.svm import SVC

# Modules import
from data_service import Data
import components.navbar as navbar
import components.command_panel as command_panel
import components.plotting as plotting
import utils.dash_reusable_components as drc
import utils.figures as figs

#import components.gymkana as gymkana


#
# INIT > scraped
#

# Classes instances
data = Data()

def generate_data(n_samples, dataset, noise):
    if dataset == "moons":
        return datasets.make_moons(n_samples=n_samples, noise=noise, random_state=0)

    elif dataset == "circles":
        return datasets.make_circles(
            n_samples=n_samples, noise=noise, factor=0.5, random_state=1
        )

    elif dataset == "linear":
        X, y = datasets.make_classification(
            n_samples=n_samples,
            n_features=2,
            n_redundant=0,
            n_informative=2,
            random_state=2,
            n_clusters_per_class=1,
        )

        rng = np.random.RandomState(2)
        X += noise * rng.uniform(size=X.shape)
        linearly_separable = (X, y)

        return linearly_separable

    else:
        raise ValueError(
            "Data type incorrectly specified. Please choose an existing dataset."
        )


layout = html.Div(
    children=[
        # .container class is fixed, .container.scalable is scalable
        ######################
        # NAVBAR HEADER
        ######################
        html.Div(
            className="banner",
            children=[
                # Change App Name here
                html.Div(
                    className="container scalable",
                    children=[
                        # Change App Name here
                        html.H2(
                            id="banner-title",
                            children=[
                                html.A(
                                    "Taxi Solver explorer",
                                    href="https://github.com/plotly/dash-svm",
                                    style={
                                        "text-decoration": "none",
                                        "color": "inherit",
                                    },
                                )
                            ],
                        ),
                        html.A(
                            id="banner-logo",
                            #children=[
                            #    html.Img(src=app.get_asset_url("dash-logo-new.png"))
                            #],
                            href="https://plot.ly/products/dash/",
                        ),
                    ],
                )
            ],
        ),
        ##########################
        # COMMAND PANEL
        ##########################
        html.Div(
            id="body",
            className="container scalable",
            children=[
                html.Div(
                    id="app-container",
                    # className="row",
                    children=[
                        html.Div(
                            # className="three columns",
                            id="left-column",
                            children=[
                                drc.Card(
                                    id="first-card",
                                    children=[
                                        ########################
                                        # BUTTONS PING AND CALL API test
                                        ########################
                                        html.Button(
                                            "ping",
                                            id="button-ping",
                                            n_clicks=0
                                        ),
                                        html.Button(
                                            "script",
                                            id="button-algo",
                                            n_clicks=0
                                        ),
                                        html.Button(
                                            "draw",
                                            id="button-draw",
                                            n_clicks=0
                                        ),
                                        html.Div(
                                            id="button-feedback",
                                            children='Enter a value and press submit'
                                        ),
                                        html.Div(
                                            id="button-feedback2",
                                            children='Enter a value and press submit'
                                        ),
                                        drc.NamedDropdown(
                                            name="Select Dataset",
                                            id="dropdown-select-dataset",
                                            options=[
                                                {"label": "Moons", "value": "moons"},
                                                {
                                                    "label": "Linearly Separable",
                                                    "value": "linear",
                                                },
                                                {
                                                    "label": "Circles",
                                                    "value": "circles",
                                                },
                                            ],
                                            clearable=False,
                                            searchable=False,
                                            value="moons",
                                        ),
                                        drc.NamedSlider(
                                            name="Sample Size",
                                            id="slider-dataset-sample-size",
                                            min=100,
                                            max=1000,
                                            step=100,
                                            marks={
                                                str(i): str(i)
                                                for i in [200, 400, 600, 800]
                                            },
                                            value=1000,
                                        ),
                                                                            
                                        drc.NamedSlider(
                                            name="Alpha",
                                            id="slider-svm-parameter-alpha-power",
                                            min=0,
                                            max=1,
                                            value=0.5,
                                            marks={
                                                i: "{}".format(0.3 ** i)
                                                for i in range(0, 1)
                                            },
                                        ),
                                        drc.NamedSlider(
                                            name="Epsilon",
                                            id="slider-svm-parameter-epsilon-power",
                                            min=0,
                                            max=1,
                                            value=0.5,
                                            marks={
                                                i: "{}".format(10 ** i)
                                                for i in range(-0, 1)
                                            },
                                        ),
                                        drc.NamedSlider(
                                            name="Gamma",
                                            id="slider-svm-parameter-gamma-power",
                                            min=0,
                                            max=1,
                                            value=0.5,
                                            marks={
                                                i: "{}".format(10 ** i)
                                                for i in range(0, 1)
                                            },
                                        ),
                                        drc.NamedSlider(
                                            name="Noise Level",
                                            id="slider-dataset-noise-level",
                                            min=0,
                                            max=1,
                                            marks={
                                                i / 10: str(i / 10)
                                                for i in range(0, 11, 2)
                                            },
                                            step=0.1,
                                            value=0.2,
                                        ),
                                    ],
                                ),
                                drc.Card(
                                    id="button-card",
                                    children=[
                                        drc.NamedSlider(
                                            name="Threshold",
                                            id="slider-threshold",
                                            min=0,
                                            max=1,
                                            value=0.5,
                                            step=0.01,
                                        ),
                                        html.Button(
                                            "Reset Threshold",
                                            id="button-zero-threshold",
                                        ),
                                    ],
                                ),
                                drc.Card(
                                    id="last-card",
                                    children=[
                                        drc.NamedDropdown(
                                            name="Kernel",
                                            id="dropdown-svm-parameter-kernel",
                                            options=[
                                                {
                                                    "label": "Radial basis function (RBF)",
                                                    "value": "rbf",
                                                },
                                                {"label": "Linear", "value": "linear"},
                                                {
                                                    "label": "Polynomial",
                                                    "value": "poly",
                                                },
                                                {
                                                    "label": "Sigmoid",
                                                    "value": "sigmoid",
                                                },
                                            ],
                                            value="rbf",
                                            clearable=False,
                                            searchable=False,
                                        ),
                                        drc.NamedSlider(
                                            name="Cost (C)",
                                            id="slider-svm-parameter-C-power",
                                            min=-2,
                                            max=4,
                                            value=0,
                                            marks={
                                                i: "{}".format(10 ** i)
                                                for i in range(-2, 5)
                                            },
                                        ),
                                        drc.FormattedSlider(
                                            id="slider-svm-parameter-C-coef",
                                            min=1,
                                            max=9,
                                            value=1,
                                        ),
                                        drc.NamedSlider(
                                            name="Degree",
                                            id="slider-svm-parameter-degree",
                                            min=2,
                                            max=10,
                                            value=3,
                                            step=1,
                                            marks={
                                                str(i): str(i) for i in range(2, 11, 2)
                                            },
                                        ),
                                        drc.FormattedSlider(
                                            id="slider-svm-parameter-gamma-coef",
                                            min=1,
                                            max=9,
                                            value=5,
                                        ),
                                        html.Div(
                                            id="shrinking-container",
                                            children=[
                                                html.P(children="Shrinking"),
                                                dcc.RadioItems(
                                                    id="radio-svm-parameter-shrinking",
                                                    labelStyle={
                                                        "margin-right": "7px",
                                                        "display": "inline-block",
                                                    },
                                                    options=[
                                                        {
                                                            "label": " Enabled",
                                                            "value": "True",
                                                        },
                                                        {
                                                            "label": " Disabled",
                                                            "value": "False",
                                                        },
                                                    ],
                                                    value="True",
                                                ),
                                            ],
                                        ),
                                    ],
                                ),
                            ],
                        ),
                        #############################
                        # PLOTS 2
                        #############################

                        html.Div(
                            id="plot-graph1",
                            children=dcc.Graph(
                                id="tuning-graph1",
                                figure=data.figure1()
                            )
                        ),
                        html.Div(
                            id="plot-graph2",
                            children=dcc.Graph(
                                id="tuning-graph2",
                                figure=data.figure2()
                            )
                        ),

                        #html.Div(
                        #    id="div-graphs",
                        #    children=dcc.Graph(
                        #        id="graph-sklearn-svm",
                        #        figure=dict(
                        #            layout=dict(
                        #                plot_bgcolor="#282b38", paper_bgcolor="#282b38"
                        #            )
                        #        ),
                        #    ),
                        #),
                        html.Div(
                            id="gym-area",
                            #children=gymkana.layout,
                        )
                    ],
                )
            ],
        ),
    ]
)


############
# BUTTON CALLBACK
############

@callback(
    Output('button-feedback', 'children'),
    Input('button-ping', 'n_clicks'),
    #State('input-on-submit', 'value')
)
def update_output(n_clicks):
    print("button ping callback")
    data.ping()
    return 'check backend connectivity {} times'.format(
        n_clicks
    )

@callback(
    Output('button-feedback2', 'children'),
    Input('button-algo', 'n_clicks'),
    Input('slider-svm-parameter-alpha-power', 'value'),
    Input('slider-svm-parameter-epsilon-power', 'value'),
    Input('slider-svm-parameter-gamma-power', 'value'),
    Input('slider-dataset-sample-size', 'value')
    # State('input-on-submit', 'value')
)
def update_output(n_clicks, alpha, epsilon, gamma, nb_games):
    print("button algo callback")

    data.ql_custom(alpha, epsilon, gamma, nb_games)
    return 'simulation nb {} and a,e,c,nbgames {} {} {} {}'.format(
        n_clicks,alpha,epsilon,gamma,nb_games
    )

@callback(
    Output('tuning-graph1', 'figure'),
    Output('tuning-graph2', 'figure'),

    Input('button-draw', 'n_clicks'),
    Input('slider-svm-parameter-alpha-power', 'value'),
    Input('slider-svm-parameter-epsilon-power', 'value'),
    Input('slider-svm-parameter-gamma-power', 'value'),
    Input('slider-dataset-sample-size', 'value')
    #State('input-on-submit', 'value')
)
def update_output(n_clicks, alpha, epsilon, gamma, nb_games):
    """ callback to re draw the graphs with user parameter tuning
        returns figures
    """
    print("button draw callback")
    data.ping()
    tuned_figures= data.dataflow(alpha, epsilon, gamma, nb_games)
    return tuned_figures[0], tuned_figures[1]
