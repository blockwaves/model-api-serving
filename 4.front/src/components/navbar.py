"""
displays header and menu and navbar links
"""
# global imports
from dash import Dash, html, dcc, Input, Output, State, callback
# import dash_bootstrap_components as dbc


# local imports
import config
import utils.dash_reusable_components as drc


# .container class is fixed, .container.scalable is scalable
layout = html.Div(
            className="banner",
            children=[
                # Change App Name here
                html.Div(
                    className="container scalable",
                    children=[
                        # Change App Name here
                        html.H2(
                            id="banner-title",
                            children=[
                                html.A(
                                    "Taxi Solver explorer",
                                    href="https://github.com/plotly/dash-svm",
                                    style={
                                        "text-decoration": "none",
                                        "color": "inherit",
                                    },
                                )
                            ],
                        ),
                        html.A(
                            id="banner-logo",
                            #children=[
                            #    html.Img(src=Dash.get_asset_url("dash-logo-new.png"))
                            #],
                            href="https://plot.ly/products/dash/",
                        ),
                    ],
                )
            ],
        ),

