"""
shows results of queries as plotted graphs using plotly
- reward
- game board
"""

from dash import dcc, html, Input, Output, callback, dash, State

layout = html.Div(
            id="div-graphs",
            children=dcc.Graph(
                id="graph-sklearn-svm",
                figure=dict(
                    layout=dict(
                        plot_bgcolor="#282b38", paper_bgcolor="#282b38"
                    )
                ),
            ),
        ),

"""
class Plot():
    def __init__(self):
        pass
        '''
        layout = html.Div()
        '''

    def plot_reward(self):
        pass

    def plot_gameboard(self):
        pass
    
"""