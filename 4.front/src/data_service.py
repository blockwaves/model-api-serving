"""
data servicing class, method to help fetch data from API
can also push data (eg, hyperparameters)
"""
import requests as rq
import pandas as pd
import json

import plotly.graph_objs as go
import plotly.express as px
import numpy as np
from sklearn import metrics

API_URL = 'http://localhost:8080'

class Data():
    def __init__(self):
        pass

    def ping(self):
        try:
            response = rq.get(API_URL + '/ping')
            print('api says hello! ', response.json())
        except Exception as e:
            print(e)

    def ql(self):
        try:
            response = rq.post(API_URL + '/qlearning')
            print("QL!")
            #print('qlearning! ', response.json())
        except Exception as e:
            print(e)
        print(type(response))
        
        return response.json()

    #slider-dataset-sample-size
    def ql_custom(self, alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000):
        try:
            url=API_URL + '/qlearning'
            response = rq.post(url, json={
                    "alpha": alpha,
                    "epsilon": epsilon,
                    "gamma=": gamma,
                    "nb_games": nb_games,
                }
            )
            print(response.status_code)
            print("QL custom!", alpha ,epsilon, gamma, nb_games)

        except Exception as e:
            print(e)
        print(type(response))
        
        return response.json()

    def parse_response(self):
        resp = self.ql_custom()
        #print(resp)
        resp = json.loads(resp)
        print(resp["steps"],resp["rewards"])

        return resp["steps"],resp["rewards"]

    def figure1(self):
        """ figure 1 is plotted at initialisation  of the app
        figures can be plotted with graph_object lib #go.Figure(data=[go.Scatter(x=[1, 2, 3], y=[4, 1, 2])]).
        """
        a,b = self.parse_response()
        print(type(a))
        c = [i for i in range(len(a))]
        figure = go.Figure(data=[go.Scatter(x=c, y=a)])
        

        return figure

    def figure2(self):
        """
        figure2 is the reward graph
        figure 2 is plotted at initialisation of the app
        """
        a,b = self.parse_response()
        print(type(b))
        c = [i for i in range(len(b))]
        figure = go.Figure(data=[go.Scatter(x=c, y=b)])

        return figure

    def figure1_custom(self, a, b):
        """ updated figure1 with custom data - after app init """
        print(type(a))
        c = [i for i in range(len(a))]
        figure = go.Figure(data=[go.Scatter(x=c, y=a)])

        return figure

    def figure2_custom(self, a, b):
        """ updated figure2 with custom data """
        a,b = self.parse_response()
        print(type(b))
        c = [i for i in range(len(b))]
        figure = go.Figure(data=[go.Scatter(x=c, y=b)])

        return figure   

    def parse_response_custom(self, _response):
        resp = _response
        #print(resp)
        resp = json.loads(resp)
        print(resp["steps"],resp["rewards"])

        return resp["steps"],resp["rewards"]

    def dataflow(self, a=0.5, e=0.5, g=0.5, n=1000):
        """method to get data from api with custom params"""
        _json_resp=self.ql_custom(a,e,g,n)
        _parsed_resp=self.parse_response_custom(_json_resp)
        fig1 = self.figure1_custom(_parsed_resp[0], _parsed_resp[1])
        fig2 = self.figure2_custom(_parsed_resp[0], _parsed_resp[1])
        return fig1, fig2

    