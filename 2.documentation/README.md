## links
- difference taxi V2 vs V3? https://github.com/openai/gym/pull/1653/files
- deep explanation of the gym environment https://medium.com/@anirbans17/reinforcement-learning-for-taxi-v2-edd7c5b76869

## lexic
- DQN: deep q-learning, an improved version of the Q learning by using a neural network to approximate the Q function.
    - alpha: alpha is the learning rate.
    - epsilon: epsilon is the probability of choosing a random action.
    - gamma: gamma is the discount factor.
- brute-force: is a smart way of doing things
- Q-learning: Q-learning is a method for learning the optimal policy for a given environment.
- Q-table: Q-table is a table of Q-values, which is a matrix of size [number of states x number of actions]
- Q-value: Q-value is the value of a state-action pair.
- recompense: recompense is the reward for a given state-action pair. Think reward as life and death, if you perform a worthy action you get a reward, if one sucks too much, it will die.
- state: state is the current state of the environment. eg, call the elevator (do nothing, start), wait for the elevator(do nothing), take the elevator(go up), hit the floor(do nothing), wait for boarding(do nothing), unboard(arrival)

### action space
- Move south
- Move north
- Move east
- Move west
- Pick-up
- Drop-off