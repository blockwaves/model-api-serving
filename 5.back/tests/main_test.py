from .context import src

# Test response type as list or single entity

from fastapi.testclient import TestClient

from src.main import app

client = TestClient(app)

data = {
    """ a dict to compare with the response """
    "username": "test@test.com",
    "password": "test",
}


def test_create_user():
    """
    test to verify user creation pipeline
    """
    response = client.post("/api/v1/users", json=data)
    assert response.status_code == 500  # should be 200
    assert response.json() == data


def test_create_token():
    """
    test to verify login and token generation

    """
    pass


def test_ping():
    """
    test with a dummy route
    """
    pass


def test_thecat():
    """
    test for file transfer

    """
    pass
