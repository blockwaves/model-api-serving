## Datasets
### remarks
datsets are csv files and should not contains special characters within columns.
 - ' ' spaces should be replaced by '_'
 - % should be replaced by 'pc'
 - [ and ] should be replaced by '' nothing

 Otherwise one would make sure parsing would escape special characters.  
 Once again no space, keep it simple (and lowercase for windows and mac enthousiasts, yes you...)

 So you should pre-process files using a notebook maybe and then load csv into the DB.
 The file should be named with the revision marking (A1, A2... B1, B2... or 0.0.1... 0.0.2... aka major.minor.patch) and the dataset name.