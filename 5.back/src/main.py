"""
main app module
CRUD operations for the database
not enforced yet, so watch out for sensitive iznogood type operations (like user doing admin stuff)
Do not overshadow 2 endpoints with the same route, it yields stupid errors
NB: Swagger UI from localhost:xxxx/docs is not an API tester in itself, it only tests endpoints schemas,
    so it will lag for calling large response body JSON, but actually the api call would be faster
NB: get_db has no bracket when called
"""

# global deps
import json
import fastapi as _fastapi
from fastapi.responses import FileResponse
import fastapi.security as _security
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
import os
import sqlalchemy.orm as _orm
from typing import List
import gym

# internal modules
import services as _services
import schemas as _schemas
import algorithms as algo
import algorithms.qlearning as ql
#import algorithms.sarsa as srs
import algorithms.montecarlo as mct
import algorithms.bruteforce as brt


app = _fastapi.FastAPI()
cwd = os.getcwd()


#
# TEST CAPABILITIES
#


@app.get("/ping", summary="Check that the service is operational")
def pong():
    """
    Sanity check - this will let the user know that the service is operational.
    It is also used as part of the HEALTHCHECK. Docker uses curl to check that the API service is still running,
    by exercising this endpoint.
    """
    return {"ping": "pong!"}


#############################################################################
# FEATURE ALGO API
#############################################################################


@app.post("/montecarlo", summary="Run the model")
def montecarlo():
    """
    Monte Carlo model
    in: policy', 'Q', 'returns', and 'nb_games'
    out: dict
    """
    mc = algo.montecarlo.fit()
    return mc


@app.post("/qlearning", summary="Run the model")
def qlearning(alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000, train_mode="manualk"):
    """
    qlearning model
    in:  env, alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000, train_mode="opti"
    out: dict
    """
    _env = gym.make("Taxi-v3").env
    #ql = algo.qlearning.play(_env, alpha=alpha, epsilon=epsilon, gamma=gamma, nb_games=nb_games, train_mode="manualk")
    ql = algo.qlearning.play(_env, alpha=alpha, epsilon=epsilon, gamma=gamma, nb_games=nb_games, train_mode=train_mode)
    #print(ql)
    ql_json = json.dumps(ql)
    #ql_json = json.dumps(ql.tolist())
    return ql_json
    #json_compatible_item_data = jsonable_encoder(ql)
    #return JSONResponse(content=ql)


@app.post("/qlearning-training", summary="train the model")
def qlearning_train():
    """
    qlearning model
    in:  env, alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000, train_mode="opti"
    out: dict
    """
    _env = gym.make("Taxi-v3").env
    qlt = algo.qlearning.play(_env, train_mode="opti")
    return qlt


@app.post("/sarsa", summary="Run the model")
def pong():
    """
    sarsa model
    in: _env, alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000, train_mode="opti"
    out: dict
    """
    _env = gym.make("Taxi-v3").env
    srs = algo.sarsa.fit(alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000)
    return srs


@app.post("/bruteforce", summary="Run the model")
def pong():
    """
    bruteforce model
    in:
    out:
    """
    _env = gym.make("Taxi-v3").env
    bf = algo.bruteforce.fit(_env)
    return {'algo': 'bruteforce'}


#############################################################################
# FEATURE USER MANAGEMENT
#############################################################################


#############################################################################
# FEATURE GET DATA FROM DATABASE AS JSON
#############################################################################


#############################################################################
# REFACTORING SPACE
#############################################################################


#############################################################################
# WIP USE CASE (LAST IMPLEMENTED)
#############################################################################


#############################################################################
# UPCOMING STAGED FEATURES WIP (TODO LIST)
#############################################################################


@app.post("/deepqlearning", summary="Run the model")
def pong():
    """
    dqn model
    in:
    out:
    """
    return {'algo': 'dqn'}
