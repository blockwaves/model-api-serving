from hashlib import algorithms_available
from ssl import ALERT_DESCRIPTION_INSUFFICIENT_SECURITY
import fastapi as _fastapi
import fastapi.security as _security
import jwt as _jwt
import sqlalchemy.orm as _orm
import passlib.hash as _hash
from sqlalchemy.sql import text

import database as _database
import models as _models
import schemas as _schemas


oauth2schema = _security.OAuth2PasswordBearer(tokenUrl="api/v1/token")

"""
insecure vvv !!!
add to dot env or other solution
"""
JWT_SECRET = "somehowajwtsecret2"


def create_database():
    """
    create db by invoking create_database() in a python3 shell:
        python3
        import services
        services.create_database()
    """
    return _database.Base.metadata.create_all(bind=_database.engine)


def get_db():
    """
    retrieves db
    """
    db = _database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

