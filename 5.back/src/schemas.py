"""
Shape model and data with Pydantic Schemas


#
# Structure:
#


class _private_objectBase(_pydantic.BaseModel):
    object: type

class objectCreate(_objectBase):
    additional_object: type
    class Config:
        orm_mode = True

class public_object(_objectBase):
    id: int
    class Config:
        orm_mode = False
"""
