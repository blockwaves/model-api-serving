import gym
import numpy as np
import random
import time
from tqdm import tqdm
import pdb
import algorithms.metrics_m
import optuna
from IPython.display import clear_output


def fit(alpha, epsilon, gamma, nb_games):
    env = gym.make("Taxi-v3").env
    env.reset()
    # For plotting metrics
    all_penalties = []
    all_games_steps_counts = []
    all_games_times = []
    all_games_rewards = []
 
    # Q TABLE
    Q_table = np.zeros([env.observation_space.n, env.action_space.n])

    for i in tqdm(range(nb_games)):
        t0 = time.time()
        state = env.reset()
        steps_count, penalties, reward = 0, 0, 0
        done = False
        rewards = []
        while not done:
            
            
            if random.uniform(0, 1) < epsilon:
                action = env.action_space.sample() # Explore action space
            else:
                action = np.argmax(Q_table[state]) # Exploit learned values

            next_state, reward, done, info = env.step(action) 
            rewards.append(reward)
            
            old_value = Q_table[state, action]
            next_max = np.max(Q_table[next_state])
            
            new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
            Q_table[state, action] = new_value

            if reward == -10:
                penalties += 1

            state = next_state
            steps_count += 1
         
        all_games_steps_counts.append(steps_count)
        all_games_times.append(time.time()-t0)
        all_games_rewards.append(np.mean(rewards))


    return_dict = {}
    return_dict["steps"] = all_games_steps_counts
    return_dict["penalties"] = penalties
    #return_dict["Q_table"] = Q_table
    return_dict["games_times"] = all_games_times
    return_dict["rewards"] = all_games_rewards
    return_dict["penalties"] = None

    return return_dict

def play(env, alpha=0.5, epsilon=0.5, gamma=0.5, nb_games=1000, train_mode="opti"):
    def objective(trial):
        results = fit(trial.suggest_float("alpha", 0, 1 ), trial.suggest_float("epsilon", 0, 1 ), trial.suggest_float("gamma", 0, 1 ), trial.suggest_int("nb_games", 10000, 30000 ) )
        return min(results["steps"])

    if train_mode == "opti":
        study = optuna.create_study(direction="minimize")
        study.optimize(objective, n_trials=5)

        alpha = study.best_params["alpha"]
        epsilon = study.best_params["epsilon"]
        gamma = study.best_params["gamma"]
        nb_games = study.best_params["nb_games"]

    results = fit(alpha, epsilon, gamma, nb_games)

    return results

    


if __name__ == '__main__':
    # -- TRAIN --
    env = gym.make("Taxi-v3").env
    env.reset()
    results = play(env)

    # -- TEST --
    env.reset()

    # Useful variables
    steps = 0
    done = None
    history = []

    # Run the game !
    while not done:
        env.render()
        time.sleep(1.5)
        steps += 1
        state = env.s
        action = np.argmax(results["Q_table"][state]) 
        next_state, reward, done, info = env.step(action) 
        import os
        os.system('clear')

    print(f"This game was resolved in { steps } steps")

    pdb.set_trace()
