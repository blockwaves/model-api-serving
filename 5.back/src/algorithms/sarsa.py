# SARSA stands for State-Action-Reward-State-Actin
# Algorithm for learning a Markov decision process policy
# The update equation for SARSA depends on the current state, current action, reward obtained, next state and next action

# Librairies
import pdb
import numpy as np
import gym
import time
from tqdm import tqdm

def fit(alpha, epsilon,gamma,total_episodes):
    # Defining utility functions to be used n the learning process
    # Function to choose the next action
    def choose_action(state):
        action = 0
        if np.random.uniform(0, 1) < epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(Q[state, :])
        return action

    # Function to learn the Q-value
    def update(state, state2, reward, action, action2):
        predict = Q[state, action]
        target = reward + gamma * Q[state2, action2]
        Q[state, action] = Q[state, action] + alpha * (target - predict)

    # Initializing the Q-matrix
    Q = np.zeros((env.observation_space.n, env.action_space.n))
    # Training the learning agent
    # Initializing the reward
    reward = 0
    # Initializing metrics array
    all_games_rewards = []
    all_games_steps_counts = []
    all_games_times = []

    # Starting the SARSA learning
    for episode in tqdm(range(total_episodes)):
        t0 = time.time()
        steps_count, reward1 = 0, 0
        rewards = []
        t = 0
        state1 = env.reset()
        action1 = choose_action(state1)

        while t < max_steps:
            # Visualizing the training
            # env.render()

            # Getting the next state
            state2, reward, done, info = env.step(action1)

            # Choosing the next action
            action2 = choose_action(state2)

            # Learning the Q-value
            update(state1, state2, reward, action1, action2)

            state1 = state2
            action1 = action2

            # Updating the respective vaLues
            t += 1
            reward += 1

            # If at the end of learning process
            next_state, reward, done, info = env.step(action1)
            if done:
                break

            # Update metrics
            rewards.append(reward1)
            steps_count += 1

        # Add updated metrics to array
        all_games_rewards.append(np.mean(rewards))
        all_games_steps_counts.append(steps_count)
        all_games_times.append(time.time()-t0)

    # Return the dicts containing the metrics updated
    return_dict = {}
    return_dict["rewards"] = all_games_rewards
    return_dict["steps"] = all_games_steps_counts
    return_dict["games_times"] = all_games_times
    return_dict["Q_table"] = Q

    return(return_dict)


if __name__ == '__main__':
    # Building the environment
    env = gym.make("Taxi-v3")

    # Defining the different hyperparamaters
    total_episodes = 10000
    max_steps = 100
    # Epsilon
    # The epsilon-greedy policy selects a random action with probability epsilon or the best known action with probability 1-epsilon
    # At epsilon=1 it will always pick the random action. This value makes the trade-off between exploration and exploitation
    epsilon = 0.5
    # Alpha
    # The learning rate determines to what extent newly acquired information ovrrides old information
    # alpha=0 will make the agent not learn anything, 1 would make the agent consider only the most recent information
    alpha = 0.85
    # Gamma
    # The discount factor determines the importance of future rewards.
    # gamma=0 makes the agent only considering current rewards, while a factor approaching 1 will make it strive for a long-term high reward.
    gamma = 0.95




# Evaluating the performance
print("Performance : ", reward/total_episodes)

# Visualizing the Q-matrix
print(Q)

pdb.set_trace()
