"""
 MonteCarlo ML algorithm
 Description: Monte Carlo solves taxi play by xx
 IN > hyperparameters
 OUT > reward/run table

"""

import gym
from tqdm import tqdm
import numpy as np
import time
import algorithms.metrics_m as mm

# -- USEFUL FUNCTIONS --
## -- Gey key,value where value is the maximum in the given dictionary
def max_dict(d):
    max_key = None
    max_val = float('-inf')

    for k, v in d.items():
        if v > max_val:
            max_key = k
            max_val = v
    
    return max_key, max_val


# -- PLAY ONE GAME -- 
def one_game(policy):
    env.reset()
    state_action_reward = []
    done = None
    steps_counter = 0
    # History (no duplicates) of seen state to avoid being stuck in a wall
    seen_states = set()

    # - Play the game - (Do steps until stuck in a wall or game done)
    while True:
        # Get the current state we are at
        current_state = env.s

        # Logging
        # print(f"Begin state { current_state }")

        # ACTION CHOICE
        # First step is random
        if steps_counter == 0:
            action = env.action_space.sample()

        # Other steps are 
        else:
            # Pick up the action to take according the policy
            action = policy[current_state] 

        # Logging
        # print(f"Action { action }")

        # Take the step
        next_state, reward, done, info = env.step(action) 

        # Increase step counter
        steps_counter += 1

        # Reward attribution and state/action/reward storing
        # If the agent hasn't move
        if current_state in seen_states:

            # Redefine a reward
            # reward =  round(-10 / steps_counter, 2)

            # Feed state action reward data structure
            state_action_reward.append((current_state,None,reward)) 

            # Logging
            # print("Agent stuck -> get go next game")
            break

        # If the game is completed, add the 
        elif done:
            state_action_reward.append((current_state,None,reward)) 
            # print("Game finished successfully -> get go next game")
            break
       
        else:
            state_action_reward.append((current_state,action,reward)) 
            # print("Store value of the step and go next step")
        
        seen_states.add(current_state)


    # Logging
    # pprint.pprint(state_action_reward)


    # - SCORE / RETURNS CALCULUS -
    score = 0
    state_action_returns = []
    isFirstIteration = True

    # Go through eact action taken and calculate a score
    for state, action, reward in reversed(state_action_reward):
        
        # Don't add the last action to the returns because it's anyway a None (in case of Fail Or Win)
        isFirstIteration = False if isFirstIteration else state_action_returns.append((state,action,score))
        
        #Score formula (even if it's the last action, we need to update te score considering the reward it gets)
        score = reward + GAMMA * score

        # Logging
        # print(f"State : { state } / Action : { action } / Reward : { reward } / Score :  { score }")
    
    rewards = [i[2] for i in state_action_reward]

    # Return the returns
    return list(reversed(state_action_returns)) , steps_counter, done, rewards

# -- CREATE A RANDOM POLICY --
def initPolicy():
    policy = {}
    for state in range(env.observation_space.n):
        policy[state] = env.action_space.sample()
    return policy

# -- INITIALIZATION OF Q(state,action) an Returns --
def initQ():
    Q = {}
    returns = {}

    for state in range(env.observation_space.n):
        Q[state] = {}
        for action in range(env.action_space.n):
            Q[state][action] = 0
            returns[(state,action)] = []

    return Q, returns

# -- FIT FUNCTION -- 
def fit(policy, Q, returns, nb_games):
    # policy = save_policy
    deltas = []
    all_games_steps_counts = [0]
    all_games_times = []
    all_games_rewards = []
    all_games_win_lose = []

    for i in tqdm(range(nb_games)):
        t0 = time.time()
        # print(f"Game { i }")
        biggest_change = 0
        state_action_returns, steps, done, rewards =  one_game(policy)    
        seen_state_action =  set()
        all_games_steps_counts.append(steps)
        all_games_rewards.append(np.mean([i[2] for i in state_action_returns]))

        if done :
            all_games_win_lose.append(1)
        else :
            all_games_win_lose.append(0)
     
        for state, action, score in state_action_returns:
            rewards.append(score)
            sa = (state,action)
            if sa not in seen_state_action:
                old_q =  Q[state][action]
                returns[sa].append(score)
                Q[state][action] = np.mean(returns[sa])
                biggest_change = max(biggest_change, np.abs(old_q -  Q[state][action] ))
                seen_state_action.add(sa)        
            deltas.append(biggest_change)
        
        # Update policy
        for state in policy.keys():
            # if max_dict(Q[state])[0] is None:  
            policy[state] = max_dict(Q[state])[0]
        
        game_time = time.time()-t0
        all_games_times.append(game_time)

    return_dict = {}
    return_dict["games_steps_counts"] = all_games_steps_counts
    return_dict["deltas"] = deltas
    return_dict["biggest_change"] = biggest_change
    return_dict["policy"] = policy
    return_dict["games_times"] = all_games_times
    return_dict["rewards"] = all_games_rewards
    return_dict["win_lose"] = all_games_win_lose

    return return_dict


if __name__ == '__main__':
    # -- TRAINING --
    # Set up env and hyperparameter
    env = gym.make("Taxi-v3").env
    GAMMA = 0.9
    nb_games = 100000
    # Initialize every variable to store simulation infos
    Q , returns = initQ()
    policy = initPolicy()

    # Train
    results = fit(policy, Q,returns, nb_games)

    # # -- TEST -- runs forever
    # env = gym.make("Taxi-v3").env
    # env.reset()
    # done = None
    # while not done:
    #     current_state = env.s
    #     next_state, reward, done, info = env.step(policy[current_state]) 
   

