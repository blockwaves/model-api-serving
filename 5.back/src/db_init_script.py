"""
script to initialize/reset database
when model is modified, the db can be "corrupted"
this is a cheap alternative to drop and create

then db must be populated again!
"""

import services
services.create_database()