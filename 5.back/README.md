# Backend API App
## Setup

### Native
* pip:
    `pip install -r requirements.txt`
* conda: 
    `conda env create -f environment.yml` (*equivalent of conda env create my-env*)
    `conda env update --file environment.yml`
### Container
* docker:
    `docker build -t greendash-api .`
    `docker run --rm -it -p 8081:8080 --name api01 greendash-api`
    

### Overview & architecture
#### Endpoints
- get/post users
- get/post roles
- get/post tgbts
- get/post analysis

#### Stack
* [fastapi](https://fastapi.tiangolo.com/tutorial/introduction/)
* [fastapi-swagger](https:localhost:8080/docs)
* **DB :** sqlite && sqlachemy
* code language: python 


#### Files

{TREE}
*update tree with `tree -LQ 3`*

#### MODULES and classes

#draw a tree of modules

app
|- __init__.py
|- main.py
    |- services.py
        |-> methods
        |
        |- schemas.py
            |-> sql tables
                |-> Users
                |-> Roles
                |-> Tgbts
        |- models.py
            |-> classes
                |-> User
                |-> Roles
                |-> Tgbts
        |- database.py

#### LIFECYCLE (you lazy debugger)
1. Python
2. Flask
3. FastAPI
4. Otherlibs
5. Code compilation
6. Main
7. Modules...


#### Troubleshooting (lazy debug)
1. no error code  > try to ping the API manually with  `curl  'http://localhost:8080/ping'`


## Code Quality
 docstring, pep8, pylint, pytest, coverage, mkdocs, swagger
### lint
### CI
### Dependancy management

### Common issues
- SWAGGER https://stackoverflow.com/questions/48749252/swagger-typeerror-failed-to-execute-fetch-on-window-request-with-get-head

### Start app
1. Import data
    Delete db and un scripts
    ```sql
     python3 db_init_script_csvdata.py
    <TGBTS10(Time=2020-12-18 10:24:00, V1_Avg_V=237.9, V2_Avg_V=238.9, V3_Avg_V=239.8,
                I1_Avg_A=313.1, I2_Avg_A=300.2, I3_Avg_A=333.2, Pt_plus_Avg_W=182800.0,
                thdI1_Avg_pc=5.032, thdI2_Avg_pc=4.197, thdI3_Avg_pc=4.448)>
    <TGBTS10(Time=2020-12-18 10:34:00, V1_Avg_V=237.8, V2_Avg_V=238.9, V3_Avg_V=239.7,
                I1_Avg_A=312.1, I2_Avg_A=300.5, I3_Avg_A=331.1, Pt_plus_Avg_W=182200.0,
                thdI1_Avg_pc=4.907, thdI2_Avg_pc=4.364, thdI3_Avg_pc=4.421)>
    <TGBTS10(Time=2020-12-18 10:44:00, V1_Avg_V=237.9, V2_Avg_V=239.1, V3_Avg_V=239.7,
                I1_Avg_A=310.2, I2_Avg_A=302.2, I3_Avg_A=330.5, Pt_plus_Avg_W=182000.0,
                thdI1_Avg_pc=4.852, thdI2_Avg_pc=4.521, thdI3_Avg_pc=4.424)>
    <TGBTS10(Time=2020-12-18 10:54:00, V1_Avg_V=238.0, V2_Avg_V=239.3, V3_Avg_V=239.7,
                I1_Avg_A=319.1, I2_Avg_A=301.8, I3_Avg_A=332.2, Pt_plus_Avg_W=184600.0,
                thdI1_Avg_pc=4.771, thdI2_Avg_pc=4.555, thdI3_Avg_pc=4.435)>
    <TGBTS10(Time=2020-12-18 11:04:00, V1_Avg_V=238.1, V2_Avg_V=239.4, V3_Avg_V=239.7,
                I1_Avg_A=312.1, I2_Avg_A=302.3, I3_Avg_A=328.7, Pt_plus_Avg_W=181900.0,
                thdI1_Avg_pc=4.855, thdI2_Avg_pc=4.602, thdI3_Avg_pc=4.468)>
    <TGBTS10(Time=2020-12-18 11:14:00, V1_Avg_V=238.3, V2_Avg_V=239.4, V3_Avg_V=239.7,
                I1_Avg_A=313.3, I2_Avg_A=302.2, I3_Avg_A=329.1, Pt_plus_Avg_W=182200.0,
                thdI1_Avg_pc=4.858, thdI2_Avg_pc=4.425, thdI3_Avg_pc=4.396)>
    <TGBTS10(Time=2020-12-18 11:24:00, V1_Avg_V=238.1, V2_Avg_V=239.4, V3_Avg_V=239.6,
                I1_Avg_A=318.2, I2_Avg_A=301.9, I3_Avg_A=329.2, Pt_plus_Avg_W=183300.0,
                thdI1_Avg_pc=4.754, thdI2_Avg_pc=4.501, thdI3_Avg_pc=4.431)>
    <TGBTS10(Time=2020-12-18 11:34:00, V1_Avg_V=237.4, V2_Avg_V=238.9, V3_Avg_V=239.2,
                I1_Avg_A=314.2, I2_Avg_A=301.9, I3_Avg_A=328.1, Pt_plus_Avg_W=182500.0,
                thdI1_Avg_pc=4.873, thdI2_Avg_pc=4.753, thdI3_Avg_pc=4.607)>
    <TGBTS10(Time=2020-12-18 11:44:00, V1_Avg_V=237.7, V2_Avg_V=239.0, V3_Avg_V=239.3,
                I1_Avg_A=311.0, I2_Avg_A=301.6, I3_Avg_A=329.1, Pt_plus_Avg_W=181700.0,
                thdI1_Avg_pc=4.883, thdI2_Avg_pc=4.61, thdI3_Avg_pc=4.539)>
    <TGBTS10(Time=2020-12-18 11:54:00, V1_Avg_V=238.1, V2_Avg_V=239.4, V3_Avg_V=239.5,
                I1_Avg_A=311.5, I2_Avg_A=301.7, I3_Avg_A=326.6, Pt_plus_Avg_W=180900.0,
                thdI1_Avg_pc=4.708, thdI2_Avg_pc=4.472, thdI3_Avg_pc=4.404)>

    ```


### psql code

```sql
-- Table: tgbt.t_measurements

-- DROP TABLE IF EXISTS tgbt.t_measurements;

CREATE TABLE IF NOT EXISTS tgbt.t_measurements
(
    id integer NOT NULL DEFAULT nextval('tgbt.t_measurements_id_seq'::regclass),
    "time" time with time zone NOT NULL,
    v1 real NOT NULL,
    v2 real NOT NULL,
    v3 real NOT NULL,
    "thdI1" real NOT NULL,
    "thdI2" real NOT NULL,
    "thdI3" real NOT NULL,
    pt_plus real NOT NULL,
    tgbt_id integer NOT NULL,
    experience_id integer NOT NULL,
    CONSTRAINT t_measurements_pkey PRIMARY KEY (id),
    CONSTRAINT fk_experience FOREIGN KEY (experience_id)
        REFERENCES tgbt.t_experiences (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT fk_tgbt FOREIGN KEY (tgbt_id)
        REFERENCES tgbt.t_tgbts (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS tgbt.t_measurements
    OWNER to postgres;
```